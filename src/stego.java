import javax.imageio.ImageIO;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.*;
import java.math.BigInteger;
import java.nio.ByteBuffer;
import java.util.*;
import java.util.List;

class Steg {

    /**
     * A constant to hold the number of bits per byte
     */
    private final int byteLength = 8;

    /**
     * A constant to hold the number of bits used to store the size of the file
     * extracted
     */
    protected final int sizeBitsLength = 32;
    /**
     * A constant to hold the number of bits used to store the extension of the
     * file extracted
     */
    protected final int extBitsLength = 64;

    private BitManipulator bitManipulator = new BitManipulator();

    public String tempPayload;

    /**
     * Default constructor to create a steg object, doesn't do anything - so we
     * actually don't need to declare it explicitly. Oh well.
     */

    public Steg() {

    }

    /**
     * A method for hiding a string in an uncompressed image file such as a .bmp
     * or .png You can assume a .bmp will be used
     *
     * @param cover_filename - the filename of the cover image as a string
     * @param payload        - the string which should be hidden in the cover image.
     * @return a string which either contains 'Fail' or the name of the stego
     * image which has been written out as a result of the successful
     * hiding operation. You can assume that the images are all in the
     * same directory as the java files
     */
    // TODO you must write this method
    public String hideString(String payload, String cover_filename) {
        // Default value to return should be "Fail", if the function is unable
        // to write an altered copy of the image with the message hidden within
        // it
        String returnString = "Fail";

        payload += " ";

        // Convert and store the binary value of the payload string
        // The following technique to convert ASCII to binary is taken from:
        // http://stackoverflow.com/a/917190/3978772

        int payloadLength = payload.length();

        //Set up a byte array to store the string prepended by 4 bits pertaining to the string size
        //byte[] bytes = new byte[(payloadLength) + 5];
        byte[] sizeBytes = ByteBuffer.allocate(4).putInt(payloadLength).array();
        //System.arraycopy(sizeBytes, 0, bytes, 0, 4);

        ArrayList<Integer> payloadList = bitManipulator.fillBits(sizeBytes, 4);

        byte[] payloadBytes = payload.getBytes();

        for (byte b : payloadBytes) {
            int val = b;
            for (int i = 0; i < byteLength; i++) {
                //binary.append((val & 128) == 0 ? 0 : 1);
                payloadList.add((val & 128) == 0 ? 0 : 1);
                val <<= 1;
            }
        }

        return hideBinaryString(payloadList, cover_filename);
    }

    // TODO you must write this method

    /**
     * The extractString method should extract a string which has been hidden in
     * the stegoimage
     *
     * @param the name of the stego image
     * @return a string which contains either the message which has been
     * extracted or 'Fail' which indicates the extraction was
     * unsuccessful
     */
    public String extractString(String stego_image) {

        //Read the string size from the first four bytes of the input
        String fileSizeString = readFiles(stego_image, 4 * byteLength);
        int fileSize = Integer.parseInt(fileSizeString, 2);

        return toASCII(readFiles(stego_image, (fileSize * byteLength) + 32));
    }

    public String toASCII(String LSBString) {

        // Default return value should be "Fail", if the function fails to load
        // an image, else it will be the message extracted from the image
        String returnString = "Fail";

        // Convert the string of LSBs to their ASCII values (taking 8 at a time,
        // for a byte)
        String ASCIIString = "";
        // The following for loop technique, to convert a binary string to an
        // ASCII string, is taken from:
        // http://stackoverflow.com/a/5453045/3978772
        //Skip the file size data found in the first four bytes
        for (int i = 32; i <= LSBString.length() - byteLength; i += byteLength) // this is a little
        // tricky. we
        // want [0, 7],
        // [9, 16], etc
        {
            ASCIIString += (char) Integer.parseInt(LSBString.substring(i, i + byteLength), 2);
        }

        // Make the string to return the message extracted from the image
        returnString = ASCIIString;

        return returnString;
    }

    // TODO you must write this method

    /**
     * The hideFile method hides any file (so long as there's enough capacity in
     * the image file) in a cover image
     *
     * @param file_payload - the name of the file to be hidden, you can assume it is in
     *                     the same directory as the program
     * @param cover_image  - the name of the cover image file, you can assume it is in
     *                     the same directory as the program
     * @return String - either 'Fail' to indicate an error in the hiding
     * process, or the name of the stego image written out as a result
     * of the successful hiding process
     */
    public String hideFile(String file_payload, String cover_image) {

        ArrayList<Integer> payloadBinary = new ArrayList<>();

        FileReader fileRead = new FileReader(file_payload);

        while (fileRead.hasNextBit()) payloadBinary.add(fileRead.getNextBit());

        return (hideBinaryString(payloadBinary, cover_image));
    }

    // TODO Merge this with other hideString method
    public String hideBinaryString(ArrayList<Integer> payload, String cover_filename) {
        // Default value to return should be "Fail", if the function is unable
        // to write an altered copy of the image with the message hidden within
        // it
        String returnString = "Fail";

        for (int i : payload) tempPayload += i;

        // Load the image using the provided path
        File file = new File(cover_filename);

        try {
            // Treat the loaded image as a BufferedImage object, allowing us to
            // get data in an image context (e.g. colour values of a pixel)
            BufferedImage image = ImageIO.read(file);

            // Loop for the image's width x height (to process each pixel in the
            // image, if required)
            outerloop:
            for (int i = 0; i < image.getHeight(); ++i) {
                for (int j = 0; j < image.getWidth(); ++j) {
                    // Get the colour values of the pixel at j, i
                    Color pixel = new Color(image.getRGB(j, i));

                    Integer[] coloursArray = getColoursArray(pixel);

                    //Loop through each colour in RGB
                    for (int coloursIndex = 0; coloursIndex < coloursArray.length; coloursIndex++) {

                        // Break out of the loop if the payload string is empty (no
                        // more characters to hide in the image)
                        if (Integer.compare(0, payload.size()) == 0)
                            break outerloop;

                        // Set the LSB of the red value of the current pixel to the
                        // character at the start of the payload string
                        coloursArray[coloursIndex] = swapLsb(coloursArray[coloursIndex], payload.get(0));

                        // Delete the character in the payload string we just stored
                        // in the "red" variable
                        payload.remove(0);

                        // Break out of the loop if the payload string is empty (no more characters to hide in the image)
                        if (Integer.compare(0, payload.size()) == 0)
                            break outerloop;

                    }

                    // Create a new Color object, using the new red, green and
                    // blue values
                    Color newPixelValue = new Color(coloursArray[0], coloursArray[1], coloursArray[2]);
                    // Replace the current pixel's value with these new values
                    image.setRGB(j, i, newPixelValue.getRGB());

                    // Check again if we are done (no more binary characters
                    // left to write) to avoid starting a new loop iteration
                    if (Integer.compare(0, payload.size()) == 0)
                        break;
                }
            }

            // Write the stego image out to file
            String fileName = "src/stego.bmp";
            ImageIO.write(image, "BMP", new File(fileName));

            // Set the return value to be the name of the altered image with the
            // string embedded within
            returnString = fileName;
        } catch (IOException e) {
            e.printStackTrace();
        }

        return returnString;
    }

    private Integer[] getColoursArray(Color pixel) {

        // Get the red, green and blue values of the current pixel and add to RGB array
        Integer[] coloursArray = new Integer[3];
        coloursArray[0] = pixel.getRed();
        coloursArray[1] = pixel.getGreen();
        coloursArray[2] = pixel.getBlue();

        return coloursArray;

    }

    public String readFiles(String stego_image, int length) {

        String LSBString = "Fail";

        // Load the image
        File file = new File(stego_image);

        // Use a list of Integers to store the values read from the image
        List<Integer> LSBList = new ArrayList<Integer>();

        try {
            // Treat the loaded image as a BufferedImage object, allowing us to
            // get data in an image context (e.g. colour values of a pixel)
            BufferedImage image = ImageIO.read(file);

            outerloop:
            for (int i = 0; i < image.getHeight(); ++i) {
                for (int j = 0; j < image.getWidth(); ++j) {

                    // Get the colour values of the pixel at j, i
                    Color pixel = new Color(image.getRGB(j, i));

                    Integer[] coloursArray = getColoursArray(pixel);

                    for (int coloursIndex = 0; coloursIndex < coloursArray.length; coloursIndex++) {

                        // Store the LSB of the red, green and blue values of the
                        // current pixel
                        coloursArray[coloursIndex] = bitManipulator.checkBit(coloursArray[coloursIndex], 0);

                        // Add the LSB of the red, green and blue values of the
                        // current pixel to the list of LSBs
                        LSBList.add(coloursArray[coloursIndex]);
                        length--;
                        if (length == 0) break outerloop;

                    }

                }
            }

            // Get the list of LSB's from the image as an array
            Object[] LSBArray = LSBList.toArray();

            // Convert the array to a single string (to get rid of '[', ']', ','
            // and
            // spacebar characters)
            LSBString = "";
            for (Object i : LSBArray) LSBString += i;

        } catch (IOException e) {
            e.printStackTrace();
        }

        return LSBString;
    }

    // TODO you must write this method

    /**
     * The extractFile method hides any file (so long as there's enough capacity
     * in the image file) in a cover image
     *
     * @param stego_image - the name of the file to be hidden, you can assume it is in
     *                    the same directory as the program
     * @return String - either 'Fail' to indicate an error in the extraction
     * process, or the name of the file written out as a result of the
     * successful extraction process
     */
    public String extractFile(String stego_image) {

        //Read the first 4 bytes of the file to obtain the payload size
        String fileSizeString = readFiles(stego_image, 4 * byteLength);
        int fileSize = Integer.parseInt(fileSizeString, 2);

        //Read the LSB pertaining to the payload file (plus 12 bytes for file extension and file size details)
        String payloadString = readFiles(stego_image, fileSize + 96);
        ArrayList<Integer> payloadList = stringToBinaryList(payloadString);

        //Get the file extension to append to end of written file name
        String fileExtension = extractFileExtension(payloadList);
        String filePath = "src/payloadOutput" + fileExtension;

        //Ignore the first 12 bytes as these relate to file size and extension
        payloadString = payloadString.substring((12 * byteLength) + 1);

        //Get the binary from the payload string
        byte[] payloadBytes = new BigInteger(payloadString, 2).toByteArray();


        //Reverse the bits in each byte as they have been read in backwards
        for (int bytesIndex = 0; bytesIndex < payloadBytes.length; bytesIndex++) {
            payloadBytes[bytesIndex] = reverseBitsByte(payloadBytes[bytesIndex]);
        }

        try {

            //Write the payload binary to a new file with the relevant extension
            DataOutputStream fileWrite = new DataOutputStream(new FileOutputStream(filePath));
            fileWrite.write(payloadBytes, 0, (fileSize / byteLength));
            fileWrite.close();

        } catch (IOException e) {

            System.out.println("Could not extract file from image. " + e);
            return "Fail";

        }

        return filePath;
    }

    //The bits are read in reverse order so they need to be put back into the correct order
    //From http://www.geekviewpoint.com/java/bitwise/reverse_bits_byte
    public byte reverseBitsByte(byte x) {

        byte y = 0;

        for (int position = byteLength - 1; position >= 0; position--) {
            y += ((x & 1) << position);
            x >>= 1;
        }
        return y;
    }

    private ArrayList<Integer> stringToBinaryList(String string) {

        //Create a new array list to store the binary from the input string
        ArrayList<Integer> arrayList = new ArrayList<>();

        //Loop through each character of the string and add the corresponding binary digit to the array list
        for (int i = 0; i < string.length(); i++) {
            if (string.charAt(i) == '0') arrayList.add(0);
            else if (string.charAt(i) == '1') arrayList.add(1);

                //Report an error if anything other than binary is found in the string
            else System.out.println("Error - not a binary string.");
        }

        return arrayList;

    }

    private int extractFileSize(ArrayList<Integer> arrayList) {

        //Return an integer calculated from the binary retrieved from bytes 0 to 4 of the input
        return Integer.parseInt(getBinaryAt(arrayList, 0, 4), 2);

    }

    private String extractFileExtension(ArrayList<Integer> arrayList) {

        //Create an empty string to hold the file extension
        String fileExtension = "";

        //Get the binary located in bytes 4 to 12 of the input (this is where the file extension binary lies)
        String binaryString = getBinaryAt(arrayList, 4, 12);

        //Loop through each byte and save the character found there
        for (int i = 0; i < binaryString.length(); i += byteLength) {
            fileExtension += (char) Integer.parseInt(binaryString.substring(i, i + byteLength), 2);
        }

        //Remove any whitespace and return
        return fileExtension.trim();

    }

    private String getBinaryAt(ArrayList<Integer> arrayList, int startByte, int endByte) {

        String binaryString = "";

        //Loop through all relevant bits and add to binary string
        for (int i = startByte * byteLength; i < endByte * byteLength; i++) {
            binaryString += arrayList.get(i);
        }

        return binaryString;

    }

    // TODO you must write this method

    /**
     * This method swaps the least significant bit of a byte to match the bit
     * passed in from the filereader
     *
     * @param byt       - the current byte
     * @param bitToHide - the bit which is to replace the lsb of the byte of the image
     * @return the altered byte
     */
    //TODO Did I get bitToHide and byt the right way round?
    // Adapted from: http://stackoverflow.com/questions/47981/how-do-you-set-clear-and-toggle-a-single-bit-in-c-c?rq=1
    public int swapLsb(int byt, int bitToHide) {
        byt ^= (-bitToHide ^ byt) & 1;
        return byt;
    }

}
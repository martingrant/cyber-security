/**
 * Created by Marty on 28/01/2016.
 */
public class Main {

    public static void main(String[] args) {

        Steg stego = new Steg();

        System.out.println(stego.hideString("helloworld", "testImage.bmp"));
        System.out.println("Extracted string: " + stego.extractString("stego.bmp"));

        // Following lines have been commented, so hiding a file doesn't overwrite the first stego image, please uncomment these
        // to test hiding and extracting a file
        //System.out.println(stego.hideFile("fileToHide.txt", "testImage2.bmp"));
        //System.out.println(stego.extractFile("stego.bmp"));
    }
}

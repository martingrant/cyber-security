import java.util.ArrayList;

//Methods utilised by both FileReader and Stego
public class BitManipulator {

    public ArrayList<Integer> fillBits(byte[] bytes, int arrayLength){

        /**
         * A constant to hold the number of bits per byte
         */
        final int byteLength = 8;

        ArrayList<Integer> bits = new ArrayList<Integer>();

        //Pad out with 0s on the left
        byte[] paddedBytes = new byte[arrayLength];
        System.arraycopy(bytes, 0, paddedBytes, paddedBytes.length - bytes.length, bytes.length);

        for(int i : paddedBytes) {
            for (int j = byteLength - 1; j >= 0; j --){
                bits.add(checkBit(i, j));
            }
        }
        return bits;

    }

    //Check if the bit is 0 or 1
    //taken from: // http://stackoverflow.com/questions/47981/how-do-you-set-clear-and-toggle-a-single-bit-in-c-c?rq=1
    public int checkBit(int number, int bit) {
        return (number >> bit) & 1;
    }


}
